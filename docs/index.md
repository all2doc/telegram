![telegram](telegram.png)

电报是迄今为止最棒的即时聊天软件，在这个自由新世界，不必自我审查（Freedom of speech）。


🧱 TG 在中国大陆必须 [翻墙](https://tingtalk.me/fq/) 后才能使用。不过，学会科学上网，难道不是当代数字公民的必备技能吗？

### Reference

[Telegram：新手指南、使用教程及频道推荐（持续更新中） | 庭说 (tingtalk.me)](https://tingtalk.me/telegram/#@chongplus)
