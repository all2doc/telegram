---
title: "Bot"
slug: "Bot"
date: 2022-05-31T15:00:56+08:00
draft: false
---


## 🤖 [Bot 推荐](https://telegram.org/faq#bots)

Bots（机器人）就像运行在 Telegram 内部的小程序。借助 [Telegram 开放的 APIs](https://core.telegram.org/api)，可以实现很多让你意想不到的功能。

[BotNews](https://t.me/BotNews)：The official source for news about the Telegram Bot API.

### 💠 内联机器人

在任意对话界面的消息编辑框，输入 [Inline Bots](https://core.telegram.org/bots/inline) 的名字，即可将 Ta 们唤醒（Just type `@inlinebots keywords` in any chat.）。

| Bot                                              | Info                                                                                                                                                                                                                                       |
| ------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [@bing](https://telegram.me/bing)                | [图片搜索 by Bing](https://www.bing.com/images)（支持中英文）。                                                                                                                                                                            |
| [@bold](https://telegram.me/bold)                | 👍 使用 [Markdown](https://tingtalk.me/markdown/) 编辑消息（有字数限制）。                                                                                                                                                                  |
| [@coub](https://telegram.me/coub)                | [Coub](https://coub.com/oftheday) 是一个视频共享网站（时长十秒的循环视频）。                                                                                                                                                               |
| [@creationdatebot](https://t.me/creationdatebot) | 获取注册 Telegram 的日期。                                                                                                                                                                                                                 |
| [@fanyi_bot](https://t.me/fanyi_bot)             | 为全世界语言提供中文翻译。                                                                                                                                                                                                                 |
| [@foursquare](https://telegram.me/foursquare)    | 帮你找到附近的餐馆或附近的地方，并将其地址发送给朋友。                                                                                                                                                                                     |
| [@gamee](https://telegram.me/gamee)              | 在群组中输入 `@gamee`，选择一个游戏，立刻和你的朋友 [在 Telegram 上玩小游戏](https://telegram.org/blog/games#ready-to-play)。                                                                                                              |
| [@gif](https://t.me/gif)                         | 👍 GIF 动图搜索，支持中文。例如 `@gif 你好`。                                                                                                                                                                                               |
| [@imdb](https://telegram.me/imdb)                | 查看影视作品在 [互联网电影资料库（IMDb）](https://www.imdb.com/)的评分。                                                                                                                                                                   |
| [@GoogleDEBot](https://telegram.me/GoogleDEBot)  | 在任意聊天框使用 Google 搜索引擎。                                                                                                                                                                                                         |
| [@like](https://t.me/like)                       | 👍 添加 emoji-based like buttons，例如 👍 / 👎。在搜索框输入 `@like`，预设一些喜欢的投票符号（最多 6 个），然后就可以在聊天框输入 `@like` 调用这些预设。                                                                                      |
| [@music](https://telegram.me/music)              | 帮你找到动听的古典音乐。                                                                                                                                                                                                                   |
| [@pic](https://telegram.me/pic)                  | [图片搜索 by Yandex](https://yandex.com/images/)（支持中英文）。                                                                                                                                                                           |
| [@QuizBot](https://t.me/QuizBot)                 | [答题机器人](https://telegram.org/blog#bot-api-and-quiz-bot)：创建一份只有单选题的考卷。[点此](https://t.me/QuizBot?start=TelegramHistory) 开始测试你对 Telegram 的了解程度。访问 [quiz.directory](https://quiz.directory/) 查看更多问卷。 |
| [@sticker](https://t.me/sticker)                 | 👍 检索所有与 Emoji 相关表情包。例如 `@sticker 😎 `。                                                                                                                                                                                        |
| [@telegraph](https://t.me/telegraph)             | 👍 登录和编辑 [Telegraph](https://telegra.ph/) 文章，并 [统计 telegra.ph 的浏览量](https://telegram.org/blog/telegraph#telegraph-api)。                                                                                                     |
| [@vid](https://t.me/vid)                         | 帮你查找 YouTube 视频（支持中文搜索）。                                                                                                                                                                                                    |
| [@vote](https://t.me/vote)                       | 投票机器人。                                                                                                                                                                                                                               |
| [@wiki](https://telegram.me/wiki)                | [维基百科](https://www.wikipedia.org/)。搜索中文条目 `@wiki zh 猫`；搜索英文条目 `@wiki en cat`                                                                                                                                            |
| [@youtube](https://telegram.me/youtube)          | 帮你查找 YouTube 视频（不支持中文搜索）。                                                                                                                                                                                                  |

### 🧱 非内联机器人

以下 Bots 不能在聊天窗口调取使用。

#### 🧡 RSS 机器人

> （如果）你不懂得 RSS，上网的效率和乐趣都要大打折扣。by [阮一峰](http://www.ruanyifeng.com/blog/2006/01/rss.html)

相比于传统的 RSS 客户端，Telegram 上的 RSS 订阅器的优点是：

- 自动记录上次浏览的位置
- 某些 RSS Bots 支持 `⚡ INSTANT VIEW`

在 [All About RSS](https://github.com/AboutRSS/ALL-about-RSS#telegram-rss-bots) 里推荐了很多 RSS Bots：

- [@FeedManBot](https://t.me/FeedManBot)
- [@TheFeedReaderBot](https://t.me/TheFeedReaderBot)：不仅可以订阅 RSS 源，还可以在 Telegram 上浏览 Twitter。
- [@Feed2Telegram_bot](https://t.me/Feed2Telegram_bot)：免费用户只有 5 条 Feeds；发送 Twitter（推特）链接，即可追踪。
- [@el_monitorro_bot](https://t.me/el_monitorro_bot)
- [@newlearner_rss_bot](https://t.me/newlearner_rss_bot)
- [@NodeRSS_bot](https://t.me/NodeRSS_bot)
- ……

以上大部分 Bots 都能免费使用，但是保不齐哪天服务器撑不住，就停止运营了，所以记得定期导出 OPML 文件作为备份。

如果有 VPS，[自己搭一个专用的 RSS Bot](https://www.google.com/search?q=telegram+rss+bot) 会是不错的选择。

#### 🎵 音乐机器人

在 Telegram 上实现点歌自由，或者像我一样建立频道，存放喜欢的歌单：[@tingtalk_fm](https://t.me/tingtalk_fm)

1. 找一个音乐机器人，例如 [@haoyybot](https://t.me/haoyybot)
2. 搜索歌名，选择歌曲，下载后转发到频道
3. 修改（Edit）歌曲信息，加入标签（方便搜索）和链接（例如 YouTube 上的 MV）

如遇到版权限制，无法下载，换用其它歌曲 Bots：

- [@ChinoNyanBot](https://t.me/ChinoNyanBot)
- [@vkm_bot](https://t.me/vkm_bot)
- [@vkmusic_bot](https://t.me/vkmusic_bot)
- [@u2bu_mp3_bot](https://t.me/u2bu_mp3_bot)

**听歌识曲**

- [@SongIDbot](https://t.me/SongIDbot)

**下载 YouTube 音频**

- [@YTAudioBot](https://t.me/YTAudioBot)

**下载音乐、歌词、视频等媒体**

- [@getmediabot](https://t.me/getmediabot)

**内置音频播放器**

- 长按住「下一首」 和「上一首」按钮可以快进和倒带。[Press and hold on the Next and Previous buttons to fast-forward and rewind.](https://telegram.org/blog/move-history#improved-audio-player)

#### 💽 DC 查询

Telegram 的服务器分布在世界各地的数据中心（[Data Center](https://core.telegram.org/api/datacenter)，简称 DC）。如何查询自己所在的数据中心（好像没啥用）：

第一步，在隐私和安全设置中，（临时）开启所有人都能查看你的头像（Profile Photos）。

第二步，选择一个查询 Bot：

| Bot                                          | Info         |
| -------------------------------------------- | ------------ |
| [@luxiaoxun_bot](https://t.me/luxiaoxun_bot) | 鲁小迅       |
| [@WooMaiBot](https://t.me/WooMaiBot)         | 干物妹！小霾 |

第三步，发送 `/dc`，即可获得你所在的数据中心。

#### 🎁 其它机器人

| Bot                                                    | Info                                                                                                                                                                                                              |
| ------------------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [@bingdict_bot](https://t.me/bingdict_bot)             | 基于 Bing 开发的中英文翻译机器人。                                                                                                                                                                                |
| [@BotsArchiveBot](https://t.me/BotsArchiveBot)         | 收集了上千个 Bots（仅限英文版）；[官网](https://botsarchive.com/index.html)。                                                                                                                                     |
| [@CorsaBot](https://t.me/CorsaBot)                     | 👍 Make Instant View from any article. 快速把文章把文章备份到 [Telegraph](https://telegra.ph/)。                                                                                                                   |
| [@cnLottery_bot](https://t.me/cnLottery_bot)           | Telegram 群组抽奖工具。                                                                                                                                                                                           |
| [@DogFatherPublicbot](https://t.me/DogFatherPublicbot) | App Store 价格监控。                                                                                                                                                                                              |
| [@githubbot](https://t.me/githubbot)                   | 推送 GitHub 仓库的动态。                                                                                                                                                                                          |
| [@GmailBot](https://t.me/GmailBot)                     | 👍 在 Telegram 上收发 📧 Gmail。                                                                                                                                                                                    |
| [@he_weather_bot](https://t.me/he_weather_bot)         | 和风天气小棉袄。另外还有 WIEN 产品的 [广州](https://t.me/cantonWeather)、[深圳](https://t.me/shamchunWeather)、[东莞](https://t.me/TungkwunWeather) 的天气速报频道。                                              |
| [@IFTTT](https://t.me/IFTTT)                           | [With this bot you can use IFTTT to link your Telegram groups or channels to more than 360 other services like Twitter and Instagram, or connected devices like Hue lights and Nest.](https://ifttt.com/telegram) |
| [@jobs_bot](https://t.me/jobs_bot)                     | This bot lists career opportunities at Telegram and accepts candidates' applications. Available at https://telegram.org/job                                                                                       |
| [@LivegramBot](https://t.me/LivegramBot)               | 👍 [不加好友也能私聊](https://telegra.ph/What-is-Livegram-Bot-03-17)，可用于收集反馈及绕开 `+86` 手机号码的限制。 因为经过一层转发，消息一旦发送，便无法删除，但有个短暂的修改期。                                 |
| [@MakeQrBot](https://t.me/MakeQrBot)                   | 发送文字，生成对应的二维码。                                                                                                                                                                                      |
| [@sssoou_bot](https://t.me/sssoou_bot)                 | Telegram 搜索，支持中文。                                                                                                                                                                                         |
| [@Stickers](https://t.me/Stickers)                     | 👍 创建属于自己的表情包。                                                                                                                                                                                          |
| [@tweet_for_me_bot](https://t.me/tweet_for_me_bot)     | 在 Telegram 上发布 Twitter 动态。                                                                                                                                                                                 |
| [@tgstogifbot](https://t.me/tgstogifbot)               | 把 Telegram 上 tgs 格式的表情包转换为 gif 格式。                                                                                                                                                                  |
| [@utubebot](https://t.me/utubebot)                     | 同时下载 YouTube 的视频和音频，不过会推送一些广告。                                                                                                                                                               |
| [@verifybot](https://t.me/verifybot)                   | 加了官方认证后，名字后面有个 ✅（[verify a big andactive official channel, bot or public group](https://telegram.org/verify)）。                                                                                   |
| [@zzzdmbot](https://t.me/zzzdmbot)                     | 真正值得买推送机器人，可以根据关键词订阅推送什么值得买精选优惠信息。                                                                                                                                              |

更多 Bots 推荐，请参阅 [Raw 博客](https://blog.rawstack.co/post/telegram-bots/) 以及 [合集网](https://www.heji.ltd/376.html)。


## 附录

### Reference

### 版权声明

本文原载于https://all2doc.com
