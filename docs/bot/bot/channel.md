在TG频道中

![image-20220707110133860](image-20220707110133860.png)

![image-20220707110203083](image-20220707110203083.png)

搜索你的机器人用户名（此处我已经完成添加）

![image-20220707110320732](image-20220707110320732.png)

点击添加即可。

![image-20220707110304397](image-20220707110304397.png)

1. 获取BOT的更新列表：

   ```
   https://api.telegram.org/bot123456789:jbd78sadvbdy63d37gda37bd8/getUpdates
   ```

   例如：

   ```
   https://api.telegram.org/bot123456789:jbd78sadvbdy63d37gda37bd8/getUpdates
   ```

2. 寻找“聊天”对象：

```
"channel_post":{"message_id":3,"sender_chat":{"id":-1001765326087,"title":"Zhelper","username":"zhelper_info","type":"channel"},"chat":{"id":-1001765326087,"title":"Zhelper","username":"zhelper_info","type":"channel"},"date":1657163128,"text":"hello"}}
```



-1001765326087

这是将BOT添加到组中时的响应示例。

1. 使用“聊天”对象的“ID”发送您的消息。