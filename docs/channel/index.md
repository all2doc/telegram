---
title: "Telegram 频道推荐"
slug: "Channel"
date: 2022-05-31T14:43:09+08:00
draft: false
---

## 📣 频道推荐

[Channels](https://telegram.org/tour/channels) 相当于公告板，是向大众传播信息的完美工具（[The perfect tool for broadcasting messages to the masses](https://telegram.org/blog/channels)），类似微信公众号，但比公众号好用得多。

通过 [Post Widget](https://core.telegram.org/widgets/posts)，你可以将频道或公共群组的任何消息嵌入到任何地方。You can embed messages from public groups and channels anywhere.

### 📢 频道主

- [Hashtags](https://telegram.org/tour/channels#hashtags)：多用 `#` （标签）给消息分类，方便快速检索（点击高亮的关键词，或者在搜索框手动输入 `# + 关键词`），然后把标签放在置顶信息里或者频道介绍里。
- 频道分析（[Channel Stats](https://telegram.org/blog/folders#channel-stats)）📈：订阅人数超过 50 人（之前是 1,000 人）的频道会有详细的统计数据分析报告（[Statistics](https://telegram.org/tour/channels#detailed-statistics)）。
- 频道可以有无数个订阅者，但是创建者只能邀请前 200 个成员到你的频道。
- 重新编辑（Edit）消息，多久之前发的 Post 都可以。
- 支持删除消息通知，减少无关紧要的动态对订阅者的干扰。出现以下通知后，可立即长按删除：
  - 更换频道置顶的消息通知 `*** pinned ***`
  - 更换频道头像的消息通知 `Channel photo updated`
  - 更改频道名字的消息通知 `Channel name was changed to ***`

**如何让你的频道或群组被更多同好知道？**

1. 打开 [@zh_secretary_bot](https://t.me/zh_secretary_bot)
2. 发送频道 ID，例如 [@tingtalk](https://t.me/tingtalk)
3. 编辑简介和标签后，即可提交收录到 [@zh_secretary](https://t.me/zh_secretary)
4. 人工审核通过后，就会在开源非盈利的 [SE-索引公告板](https://t.me/zh_secretary) 被更多同好看到啦

相似的索引机器人还有 [@PolarisseekBot](http://t.me/PolarisseekBot)。

**2020 年 9 月 30 开始，电报频道原生支持评论功能（[Channel Comments](https://telegram.org/blog/filters-anonymous-admins-comments#channel-comments)）**

首先要 [在频道的设置里绑定一个群聊（Group）](https://telegram.org/blog/privacy-discussions-web-bots#broadcasts-meet-group-chats)，频道中的每条新帖子（new post）都会自动转发到该群组并被置顶（Pin）。

频道发送消息后，有两个评论入口：

- 频道：点击 `Leave a comment` 即可进入留言板（无需加入讨论组）。
- 群组：
  - 第一层评论：引用（Reply）回复对应的频道消息。
  - 第二层评论：接龙引用第一层评论。
  - 第 N 层评论：以此类推。

通过 [@LikeComBot](https://t.me/LikeComBot) 给频道的消息下增加 Emoji 按钮，例如 👍、👎、😐。

### 🔔 订阅者

- [Subscriber Privacy](https://telegram.org/tour/channels#subscriber-privacy)：关注者无法得知频道创建者（creator）是谁，也无法得知谁关注了这个频道，但是频道主知道谁关注了频道。
- 若用户在 24 小时内访问超过 200 个群组或频道的链接（点击打开就算访问，不需要加入），就会被打入冷宫 24 小时。禁闭期间，无法通过链接访问新的群组或频道（点击链接一直转圈而无法访问）。
- 频道/超级群组的关注上限是 500 个（具体数字未得到官方的求证），但是限制是一定存在的，因为限制提示语出现在官方翻译页面：抱歉，您已加入太多频道/超级群组。请先退出一些频道/超级群组后再加入。（[Sorry, you have joined too many channels and supergroups. Please leave some before joining this one.](https://translations.telegram.org/zh-hans/tdesktop/groups_and_channels/lng_join_channel_error)）

**去哪里找钟意的频道（Channel），群组（Group）和机器人（Bot）呢？**

☝️ 在 Telegram 内直接搜索关键词，但中文搜索识别较差。例如，「庭说」的频道是 https://t.me/tingtalk

- 搜索英文 `tingtalk`（`t.me/` 后面的字符就是 ID），可以准确识别。
- 搜索中文 `庭说`，可能无法识别。

✌️ 在 [Google](https://www.google.com/search?q=site:tingtalk.me) 上搜索，配合一些 [Google 搜索技巧](https://tingtalk.me/search-tips/)：

- 搜索结果较少：`关键词 + site:t.me`，例如 [电子书 site:t.me](https://www.google.com/search?q=电子书 site:t.me)
- 搜索结果较多：`关键词 + telegram 及其别称`，例如：[电子书 telegram OR 电报 OR tg](https://www.google.com/search?q=电子书 telegram OR 电报 OR tg)

这也证明了 Telegram 的内容是可以被 Google 等搜索引擎抓取的。反观国内的互联网江湖，各自割据，搞得网民苦不堪言。就拿微信来说，你不能在 Google 或者百度搜到公众号文章，这也是庭说另开一个独立博客的原因。

也意味着如果你没有做好隐私保护，请不要在公开频道或群组发言，小心不怀好意的 [网络蜘蛛](https://zh.wikipedia.org/zh-hans/網路爬蟲) 爬到你身上。

👌 Telegram 搜索引擎（非官方），可能包含不少 NSFW 内容。

- 索引机器人
  - [@zh_secretary_bot](http://t.me/zh_secretary_bot) 👍（支持中文搜索）
  - [@PolarisseekBot](http://t.me/PolarisseekBot) 👍（支持中文搜索）
  - [@hao1234bot](http://t.me/hao1234bot)
  - [@hao6bot](http://t.me/hao6bot)
- 网页版
  - [Lyzem Search](https://lyzem.com/)
  - [名刀电报搜索](https://xtea.io/ts.html#gsc.tab=0)
  - [sssoou.com](http://www.sssoou.com/)
  - [Telegram 公眾索引系統](https://tgtw.cc/)
  - [tlgrm](https://tlgrm.eu/stickers)：只支持用英文关键词搜索

**如何通过 RSS 订阅 Telegram 频道**

有些用户觉得 Telegram 用手机号码注册不安全，但是又想第一时间获得 Telegram 公开频道的更新，那么可以 [借助 RSSHub 生成电报公开频道的 RSS 订阅链接](https://docs.rsshub.app/social-media.html#telegram)，例如：

```
https://rsshub.app/telegram/channel/tingtalk
```

只要把 `tingtalk` 替换成其他公共频道的 Permanent link（永久链接）后缀即可。

须知参差多态，乃是电报之福。术业有专攻，**欢迎向我推荐其它领域的优质频道**：

1. 在 Telegram 搜索 [@tingbot](https://t.me/tingbot)
2. 简单说明推荐理由
3. 优质频道将会更新在这篇《电报教程》里，让好内容得到更多的展现

以下是我收集的频道，不代表同意其观点，也许为了丰富文章内容。如果你发现某些频道开始「作恶」了或者失效了，请联系 [@tingbot](https://t.me/tingbot) 从这个列表中删除。

2021 年，你需要多运动，多吃蔬果，偶尔听 [播客](https://podcasts.cosmosrepair.com/)，放下手机早点睡觉，少看鸡零狗碎的消息。

**如何加入频道**
方法一：直接点击频道的名字，例如 [庭说](https://t.me/tingtalk)，浏览器会跳转到 Telegram 客户端并进入该频道
方法二：复制频道的 ID，例如 `tingtalk`，粘贴在 Telegram 客户端首页的 🔍 搜索框，在搜索结果中找到该频道

#### ✈️ 电报

##### 官方频道

| 频道                                                                  | 详情                                                                                                                                              |
| --------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Telegram News](https://t.me/telegram)                                | 👍 电报官方新闻频道。                                                                                                                              |
| [Durov's Channel](https://t.me/durov)                                 | 👍 杜罗夫（Telegram 创始人和 CEO）的频道。                                                                                                         |
| [Telegram Tips](https://t.me/TelegramTips)                            | 👍 电报小贴士（Tips）官方频道。                                                                                                                    |
| [Telegram APKs for Android](https://t.me/TAndroidAPK)                 | Official channel for Telegram Android APKs. You can also download them [here](https://telegram.org/dl/android/apk).                               |
| [Telegram for macOS Updates](https://t.me/macos_stable_updates_files) | This channel publishes release builds for [Telegram macOS](https://macos.telegram.org/).                                                          |
| [Telegram Designers](https://t.me/designers)                          | 向电报提你想要的功能 [@design_bot](https://t.me/design_bot)                                                                                       |
| [BotNews](https://t.me/BotNews)                                       | The official source for news about the Telegram Bot API.                                                                                          |
| [Telegram Contests](https://t.me/contest)                             | Here we announce Telegram coding contests in Android Java, iOS Swift, JS, C/C++.                                                                  |
| [Desktop Themes Channel](https://t.me/themes)                         | [电脑客户端主题创建指引](https://telegra.ph/Create-Theme-Desktop-FAQ) \| [Custom Themes 的简单介绍](https://telegram.org/blog/android-themes)     |
| [Android Themes Channel](https://t.me/AndroidThemes)                  | [安卓客户端主题创建指引](https://telegra.ph/Create-Theme-Android-FAQ) \| 更多技术细节参阅 [Custom Cloud Themes](https://core.telegram.org/themes) |
| [Telegram Auditions](https://t.me/TelegramAuditions)                  | 加入 Telegram Support Force，帮扶 Telegram 做大做强，详情参阅这份 [Initiative](https://tsf.telegram.org/)。                                       |
| [ISIS Watch](https://t.me/ISISwatch)                                  | 电报官方反恐频道：[每日汇报有多少恐怖组织相关的频道被封了](https://t.me/isiswatch/2)。                                                            |

此外，Telegram 上也有 [国家或地区的领导人官方频道](https://t.me/durov/148)。

##### 用户创建

| 频道                                               | 详情                                                                                                                                                               |
| -------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [TGgeek](https://t.me/TGgeek)                      | 👍 TG 极客：分享 Telegram 使用技巧、重要资讯、常见问答、中文汉化、版本更新等信息。                                                                                  |
| [电报小助手](https://t.me/dbxzs)                   | 用简体中文同步翻译官方 [@TelegramTips](https://t.me/TelegramTips) 中的小技巧。                                                                                     |
| [Trending Stickers](https://t.me/TrendingStickers) | Telegram 又新增了哪些表情包。                                                                                                                                      |
| [紙飛機](https://t.me/tgflight)                    | 欢迎搭乘纸飞机，Porsche 和你聊聊 Telegram 的大小事。[播客 RSS 订阅链接](https://feeds.buzzsprout.com/837646.rss)。                                                 |
| [Anti Revoke Plugin](https://t.me/AntiRevoke)      | Telegram 本地消息防撤回插件，安全性未知，只支持 Windows 32 位系统。[GitHub 项目地址](https://github.com/SpriteOvO/Telegram-Anti-Revoke/blob/master/README-CN.md)。 |

##### 电报导航

- [SE-索引公告板](https://t.me/zh_secretary) `zh_secretary`
  👍 Telegram 中文圈资源索引服务（包含 NSFW）。
- [北极星搜索登记板](https://t.me/PolarisseekIndex) `PolarisseekIndex`
- [电报指南 & 精品排行榜](https://t.me/TgTrillion) `TgTrillion`
- [CN 导航](https://t.me/CN_DH) `CN_DH`
  简单好记的中文多功能公益导航频道。
- [Tg Tips](https://t.me/Tg1230) `Tg1230`
  瞭望台旗下 TG 电报引航：电报操作、频道、广播、群组的信息库。
- [電報新群推送 Telegram Group Links](https://t.me/linkpush) `linkpush`
  本頻道是新群推送頻道一般只收錄剛剛建立的群組或者人數少於 150 的群組。

#### 🦠 疫情

| 频道                                                             | 详情                                                                                   |
| ---------------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| [2019-nCoV 疫情实时播报](https://t.me/nCoV2019)                  | 👍 COVID-19 中文消息 by NFNF。                                                          |
| [Coronavirus Info](https://t.me/corona)                          | 各国官方疫情通报频道列表（A list of official channels with information on COVID-19）。 |
| [Financial Times: Coronavirus news](https://t.me/FinancialTimes) | COVID-19 英文消息 by 金融时报。                                                        |

#### 📰 新闻

在一个后真相时代，要分清事实和观点:

- 对于事实，要有多个独立信源交叉验证。
- 对于观点，要注意论述逻辑和因果关系。

| 频道                                                      | 详情                                                                                                                                                                                                                                                                                                                                                                         |
| --------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [看鉴中国 OutsightChina](https://t.me/OutsightChina)      | 👍 一个健康的社会，不该只有一种声音。看鉴中国，每天聚焦一则关于中国的新闻事件，带你对比来自中外不同媒体多元的、不一样的观点。                                                                                                                                                                                                                                                 |
| [乌鸦观察](https://t.me/bigcrowdev)                       | 👍 不定期推送新闻和杂谈。                                                                                                                                                                                                                                                                                                                                                     |
| [竹新社](https://t.me/tnews365)                           | 7×24 不定时编译国内外媒体的即时新闻报道。                                                                                                                                                                                                                                                                                                                                    |
| [有据](https://t.me/chinafactcheck)                       | [China Fact Check](https://chinafactcheck.com/) 是一个专注于对中文国际资讯进行事实核查的计划，是基于志愿和网络协作原则的事实核查计划，努力连接大学、媒体和平台三方力量。                                                                                                                                                                                                     |
| [新闻实验室](https://t.me/newslab2020)                    | 推荐订阅方可成老师的 [Newsletter](https://sibforms.com/serve/MUIEABj_H1nZN9Jj5HLcOE61NoO8So-r5phaSIn4ZUqqLv_WdJP-bpiAazMxwp2uOWb-aeF-hiOScIzPxwovvRjNYF6METjZVOafOAQANa9tnIeocTm6fv6RurLCTUVGzKrjBrhtIj9q2wVOmIYoltrV310MEOEi-vg3E0uPES0voX-FjBRHd4mPx0woiIsLiZ6-YXnI80PyhpUH)。[微信公众号文章备份](https://github.com/Newslab2020/Contents/blob/master/wechataccount.md)。 |
| [南方周末](https://t.me/infzm)                            | 在这里，读懂中国。非官方。                                                                                                                                                                                                                                                                                                                                                   |
| [iDaily](https://t.me/idaily_rss)                         | [每日环球视野](http://idai.ly/)。                                                                                                                                                                                                                                                                                                                                            |
| [新周刊](https://t.me/neweekly)                           | 一本杂志和一个时代的体温。                                                                                                                                                                                                                                                                                                                                                   |
| [南都观察](https://t.me/nanduguancha_rss)                 | RSS 地址：https://www.nanduguancha.cn/rss                                                                                                                                                                                                                                                                                                                                    |
| [新闻联播（文字版）](https://t.me/cctv_news_official)     | 《新闻联播》是中国中央电视台每日在北京时间晚间 19:00 播出的一個重点时政新闻节目，于 1978 年 1 月 1 日启播。                                                                                                                                                                                                                                                                  |
| [中国数字时代消息推送](https://t.me/cdtchinesefeed)       | 致力于聚合「中国的社会与政治新闻，和它在世界上的新兴的角色」有关的报道和评论。                                                                                                                                                                                                                                                                                               |
| [多数派Masses](https://t.me/masses2020)                   | 我们是一群反对资本主义、反对帝国主义、反对父权制、反对一切压迫和宰制的青年。[Matters 的创作空间站](https://matters.news/@masses2020) \| [Newsletter](https://www.masseshere.com/订阅电子报/)                                                                                                                                                                                 |
| [60 秒读懂世界](https://t.me/SharedResources)             | 来自 60 秒读懂世界公众号。                                                                                                                                                                                                                                                                                                                                                   |
| [突发新闻](https://t.me/breakingnews_t)                   | 突发新闻推送服务（简体中文）。                                                                                                                                                                                                                                                                                                                                               |
| [NFW](https://t.me/NewsFW)                                | News for Work, Not for Work.                                                                                                                                                                                                                                                                                                                                                 |
| [电报时报](https://t.me/times001)                         | 提供全天候热点中国及国际新闻，涵盖突发新闻、时事、财经、娱乐、体育，评论、杂志和博客等。                                                                                                                                                                                                                                                                                     |
| [蘋果日報](https://t.me/appledailyhk)                     | [Apple Daily](https://hk.appledaily.com/) 为香港上市公司壹传媒旗下繁体中文报纸，由大股东黎智英所创立，被民主派支持者普遍认为是香港目前唯一未被「染红」的媒体。by [维基百科](https://zh.wikipedia.org/zh-cn/蘋果日報_(香港))                                                                                                                                                  |
| [台湾 中央社 香港 苹果日报](https://t.me/ttww_rss)        | 如题。                                                                                                                                                                                                                                                                                                                                                                       |
| [NGOCN](https://t.me/ngocn01)                             | [NGOCN](https://ngocn2.org/) 是一家中国独立媒体，非营利性质，致力向公众提供进步、负责任且多元的纪实性内容，目前由认同其理念志愿者运营。                                                                                                                                                                                                                                      |
| [中华人民共和国外交部发言人表态](https://t.me/ChinaMOFAS) | 外交部负责处理中华人民共和国政府与世界其他国家政府及政府间国际组织的外交事务。                                                                                                                                                                                                                                                                                               |
| [端傳媒 Initium Media](https://t.me/the_InitiumMedia)     | 由程式自動獲取並推送端傳媒 RSS 所有文章，链接至官网。                                                                                                                                                                                                                                                                                                                        |
| [端传媒 RSS](https://t.me/theinitium_rss)                 | 链接至 Telegraph 和官网。RSS 地址：https://rsshub.app/initium/latest/zh-hans                                                                                                                                                                                                                                                                                                 |
| [端传媒](https://t.me/lnitiumMedia)                       | 每日推送端传媒（付费）文章.pdf。手头宽裕，还是 [付费购买端会员](https://theinitium.com/subscription/offers/) 或购买 [新闻通讯 Newsletter](https://i.init.shop/collections/newsletter/products/daily-newsletter-original)。                                                                                                                                                   |

🌐 **国外媒体（简体中文）**

| 频道                                                       | 详情                                                                                                                                                                                                                                                          |
| ---------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [纽约时报中文网](https://t.me/niuyueshibao_rss)            | 👍 [The New York Times (NYT)](https://cn.nytimes.com/) 创刊于 1851 年，世界上最著名的报纸之一。美国严肃报刊的代表，获得过 122 项普利策奖，是获奖最多的媒体。                                                                                                   |
| [BBC 中文网](https://t.me/bbczhongwen_rss)                 | [BBC News](https://www.bbc.com/zhongwen/simp) 是世界最大的公共广播公司，位于英国，资金主要来自英国国民缴纳的电视牌照费，是一家独立运作的公共媒体（非商业媒体，也不由英国政府控制）。                                                                          |
| [联合早报](https://t.me/zaobaosg)                          | [zaobao.sg](http://zaobao.sg/) 早报 + 晚报 + 新明新闻。                                                                                                                                                                                                       |
| [路透中文网](https://t.me/lutouzhongwen_rss)               | [Reuters](https://cn.reuters.com/) 世界三大通讯社之一，成立于 1851 年，总部位于英国伦敦。                                                                                                                                                                     |
| [德国之声](https://t.me/dw_rss)                            | [Deutsche Welle (DW)](https://www.dw.com/zh/) 按德国公法设立的国际化公共媒体，从联邦政府获得拨款，总部位于波恩和柏林。                                                                                                                                        |
| [澳大利亚广播公司](https://t.me/abc_rss)                   | [Australian Broadcasting Corporation (ABC)](https://www.abc.net.au/news/chinese/) 是澳大利亚的国家公共广播机构，它由政府出资，向澳大利亚和海外提供电台、电视、互联网服务。总部设在悉尼。                                                                      |
| [法国国际广播电台](https://t.me/rfi_rss)                   | [Radio France Internationale (RFI)](https://www.rfi.fr/cn/) 是法国专责世界大部分地区之国际广播的电台广播机构，现隶属法国国营国际广播公司法国世界媒体旗下。by [维基百科](https://zh.wikipedia.org/zh-cn/法国国际广播电台)                                      |
| [美国之音中文网](https://t.me/meiguozhiyin_rss)            | [Voice of America (VOA)](https://www.voachinese.com/) 成立于 1942 年 2 月，是美国政府对外设立和资助的国有非军事国际广播宣传喉舌，由美国国际媒体署管理，旗下拥有广播电台与电视台，总部座落在首都华盛顿。by [维基百科](https://zh.wikipedia.org/zh-cn/美国之音) |
| [华尔街日报](https://t.me/wsj_rss)                         | RSS 地址：https://feedx.net/rss/wsj.xml                                                                                                                                                                                                                       |
| [俄罗斯卫星通讯社新闻](https://t.me/ru_rss)                | [Sputnik](http://sputniknews.cn/) 是俄罗斯政府控制的新闻机构今日俄罗斯媒体集团于 2014 年 10 月开通的新闻通讯社、新闻网站、广播电台与媒体新闻中心。by [维基百科](https://zh.wikipedia.org/zh-cn/卫星通讯社)                                                    |
| [韩国新闻](https://t.me/korea_rss)                         | [朝鲜日报](https://cnnews.chosun.com/) + [中央日报中文版](http://chinese.joins.com/)                                                                                                                                                                          |
| [日本新闻](https://t.me/jp_rss)                            | [共同网](https://china.kyodonews.net/) + [朝日新闻中文网](https://asahichinese-j.com/) + [日本经济新闻中文版](https://cn.nikkei.com/)                                                                                                                         |
| [双语新闻](https://t.me/shuangyunews_rss)                  | [纽约时报双语新闻](https://cn.nytimes.com/) + [中国日报网英语点津](http://language.chinadaily.com.cn/)                                                                                                                                                        |
| [Twitter Subscription](https://t.me/twitter_subscriptions) | 搬运以下 Twitter 账号：BBC News 中文、DW 中文- 德国之声、国际特赦组织中文、纽约时报中文网。                                                                                                                                                                   |
| [新闻播报 PDF](https://t.me/news_pdf)                      | 每天为大家送来 NYT 和 BBC 的新闻 PDF。                                                                                                                                                                                                                        |
| [What's News](https://t.me/whatsnws)                       | 推送各种英文外刊和杂志的 PDF。                                                                                                                                                                                                                                |

以上部分介绍来自西方媒体查一查。查询可信度和倾向性，请安装 [浏览器插件](https://chrome.google.com/webstore/detail/西方媒体查一查/bpejcaojjipcgcnjkfmnkhokdpimcmij)，或者访问 [微信小程序](https://minapp.com/miniapp/4395/)。

**国家 & 发言人（已认证）**

| 频道                                    | 详情     |
| --------------------------------------- | -------- |
| [Gov.sg](https://t.me/Govsg)            | 新加坡。 |
| [Donald Trump Jr](https://t.me/TrumpJr) | 特朗普。 |

💸 **财经新闻**

| 频道                                     | 详情                                                                                                                 |
| ---------------------------------------- | -------------------------------------------------------------------------------------------------------------------- |
| [财经快讯](https://t.me/fnnew)           | 全球财经资讯 24 小时不间断直播。                                                                                     |
| [FT 中文网](https://t.me/ftzhongwen_rss) | [Financial Times](https://m.ftchinese.com/)（金融时报）创刊于 1888 年，编辑总部位于伦敦，2015 年被日本经济新闻收购。 |

#### 💾 科技

| 频道                                                 | 详情                                                                                                                                                                   |
| ---------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Solidot](https://t.me/solidot)                      | 👍 奇客的资讯，重要的东西。                                                                                                                                             |
| [Readhub](https://t.me/readhub_cn)                   | 👍 [readhub.cn](https://readhub.cn/) 非官方 RSS 推送频道。                                                                                                              |
| [Newlearnerの自留地](https://t.me/NewlearnerChannel) | 👍 不定期推送 IT 相关资讯。                                                                                                                                             |
| [Appinn Feed](https://t.me/appinnfeed)               | 👍 分享免费、小巧、实用、有趣、绿色的软件。                                                                                                                             |
| [少数派](https://t.me/sspai)                         | 👍 少数派致力于更好地运用数字产品或科学方法，帮助用户提升工作效率和生活品质。                                                                                           |
| [科技爱好者周刊](https://t.me/scitech_fans)          | 👍 记录每周值得分享的科技内容，周五发布；非官方频道。[科技爱好者周刊合集](https://www.yuque.com/ruanyf/weekly)。                                                        |
| [TestFlight 科技花](https://t.me/TestFlightCN)       | 发布科技新闻、App 测试版链接、软件使用相关话题。                                                                                                                       |
| [Hacker News](https://t.me/hacker_news_feed)         | Top stories from [news.ycombinator.com](http://news.ycombinator.com/) (with 100+ score).                                                                               |
| [V2EX - 最新/最热主题](https://t.me/V2EX_topic)      | V2EX 是创意工作者们的社区，可以分享生活和事业。                                                                                                                        |
| [科技圈的日常](https://t.me/misakatech)              | 科技圈内的大事小事。                                                                                                                                                   |
| [Telegram 中文 NEWS](https://t.me/YinxiangBiji_News) | [聪聪](hhttps://congcong0806.github.io/2018/04/24/Telegram/) 的频道：提供印象笔记、Telegram、科学上网等新闻。[Telegram 知识汇总](https://t.me/YinxiangBiji_News/954)。 |
| [每日消费电子观察](https://t.me/CE_Observe)          | 不公正，不客观，不理性。                                                                                                                                               |
| [cnBeta](https://t.me/cnbeta_com)                    | [cnBeta.COM](http://cnbeta.com/) 中文业界资讯站是一个提供 IT 相关新闻资讯、技术文章和评论的观点的中文网站。                                                            |
| [IT 之家](https://t.me/ithome_rss)                   | RSS 地址：https://www.ithome.com/rss/                                                                                                                                  |
| [APPDO 数字生活指南](https://t.me/appdodo)           | 优质数字生活指南，传递数码生活和设计理念。                                                                                                                             |
| [VPS 信号旗播报](https://t.me/vps_xhq)               | 关注 VPS 和通信自由。                                                                                                                                                  |
| [硬核小卒](https://t.me/yinghexiaozu)                | 分享优质的科技/商业资讯。                                                                                                                                              |
| [知乎日报](https://t.me/zhihuribao_rss)              | 越来越难用的问答网站。                                                                                                                                                 |
| [Daily Tech News](https://t.me/DailyTechNewsCN)      | 每日科技新闻。                                                                                                                                                         |
| [每日 AWESOME 观察](https://t.me/awesomeopensource)  | 每日更新分享最炫酷的开源项目。                                                                                                                                         |
| [LetITFly News](https://t.me/LetITFlyW)              | 主题包括但不限于 Android、Windows、Web、消费电子相关，吹水为主。                                                                                                       |
| [Science](https://t.me/science)                      | Science News channel, videos and articles - international project, 35+ countries.                                                                                      |
| [OnePlus](https://t.me/OnePlus)                      | Everything OnePlus.                                                                                                                                                    |
| [老毛子 Padavan 固件发布](https://t.me/pdcn1)        | 一个路由器固件。                                                                                                                                                       |

**科技互联网**

- [即刻精选](https://t.me/jike_read) `jike_read`
  即刻精选，以及相关讨论。这里是即友们的 TG 自留地。

**Apple**

- [AppleGuide](https://t.me/AppleBuyersGuide) `AppleBuyersGuide`
  [小胖](https://littlefat.cn/) 的苹果产品购买指南，更系统请查看 [AppleGuide.cn](https://appleguide.cn/关于本站)，不断完善中。
- [果核 Apple Nuts](https://t.me/AppleNuts) `AppleNuts`
  一个果粉（[Hackl0us](https://hackl0us.com/)）的闲言碎语， 用来推送苹果（Apple） 相关的技术、新闻资讯、技巧、产品/软件心得体会等。
- [AppPie](https://t.me/AppPie) `AppPie`
  Apple 相关的数字生活指南。
- [iOS 限免与优质应用推荐](https://t.me/iosblackteckapp) `iosblackteckapp`
  免费使用正版应用，以及分享 iOS 各种高效实用应用与实用黑技巧。
- [iOS Releases](https://t.me/iOSUpdates) `iOSUpdates`
  iOS, TvOS and watchOS signing status updates. This channel will notify you when apple starts or stops signing a firmware version.

**Android**

- [问道](https://t.me/mdqwsf) `mdqwsf`
  该频道 apk 为个人汉化而来。

**软件**

- [简悦 - SimpRead](https://t.me/simpread) `simpread`
  让你瞬间进入沉浸式阅读的 [Chrome 扩展](https://chrome.google.com/webstore/detail/simpread-reader-view/ijllcpnolfcooahcekpamkbidhejabll)，还原阅读的本质，提升你的阅读体验。
  希望做一些让这个世界变得更美好的小事。by Kenshin
  [网站](http://ksria.com/simpread) | [订阅中心](https://simpread.pro/subscribe)

#### 📚 博主

| 频道                                                      | 详情                                                                                                                                                                                                                                                                                                                 |
| --------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [庭说](https://t.me/tingtalk)                             | 👍 第一时间获取博客的更新通知以及碎片化思考。                                                                                                                                                                                                                                                                         |
| [庭说 - 唠叨频道](https://t.me/tingtalk_all)              | @tingtalk_all 发布一些主频道 @tingtalk 之外的增量更新以及碎片化思考。                                                                                                                                                                                                                                                |
| [小破不入渠](https://t.me/forwardlikehell)                | 👍 科技评论人 Jesse Chan，博客是 [大破进击](https://jesor.me/)。                                                                                                                                                                                                                                                      |
| [一天世界](https://t.me/yitianshijie)                     | 👍 [一天世界](https://yitianshijie.net/)，昆乱不挡。不鸟万如一主理。IPN 出品。                                                                                                                                                                                                                                        |
| [caoz 的梦呓](https://t.me/caozsay)                       | 👍 认识曹政之后，感觉互联网终于入门了。by Fenng                                                                                                                                                                                                                                                                       |
| [ZUOLUOTV](https://t.me/zuoluotvofficial)                 | 👍 科技 / 旅行 / 摄影 / 生活方式 / [博客](https://luolei.org/)                                                                                                                                                                                                                                                        |
| [不求甚解](https://t.me/fakeye)                           | 👍 [Newlearnerの自留地](https://t.me/NewlearnerChannel) 编辑；设计师 [oooooohmygosh](https://space.bilibili.com/38053181) 的代言人。                                                                                                                                                                                  |
| [小道消息](https://t.me/WebNotes2)                        | 大道无形，小道消息；[公众号备份站点](https://hutu.me/)。                                                                                                                                                                                                                                                             |
| [卖桃者说](https://t.me/mactalk)                          | 博客是 [MacTalk](http://macshuo.com/)：池建强的随想录关注技术和人文。                                                                                                                                                                                                                                                |
| [数字移民](https://t.me/shuziyimin)                       | 无法肉身移民的情况下，在数字生活上追求一定的自由；[博客](https://blog.shuziyimin.org/)。                                                                                                                                                                                                                             |
| [Real Spencer Woo](https://t.me/realSpencerWoo)           | 开发者 / 设计师 / 少数派 / 学生 / [博客](https://blog.spencerwoo.com/)。                                                                                                                                                                                                                                             |
| [Sukka's Notebook](https://t.me/SukkaChannel)             | Belongs to [Hexo](https://github.com/hexojs) dev team / [博客](https://blog.skk.moe/)。                                                                                                                                                                                                                              |
| [扫地僧笔记](https://t.me/lover_links)                    | 每天所见所闻所想，是个树洞。                                                                                                                                                                                                                                                                                         |
| [一方天地](https://t.me/world2us)                         | 心留一方天地，世界依旧美好。                                                                                                                                                                                                                                                                                         |
| [湾区日报](https://t.me/wanqu_official)                   | 关注创业与技术，不定期推送 5 篇优质英文文章。                                                                                                                                                                                                                                                                        |
| [海龙说](https://t.me/haotalk)                            | 牢记梦想，自然生长。by [郝海龙的博客](https://haohailong.net/)                                                                                                                                                                                                                                                       |
| [荔枝木](https://t.me/lychee_wood)                        | 这个世界很复杂，我尝试着去理解它。                                                                                                                                                                                                                                                                                   |
| [KAIX.IN](https://t.me/kaix_in)                           | 思考碎片，[博客](https://kaix.in/) 更新。                                                                                                                                                                                                                                                                            |
| [TSBBLOG](https://t.me/tsbblog)                           | [影子的博客](https://tsb2blog.com/)：独立观察及记录。                                                                                                                                                                                                                                                                |
| [AK 讲废话](https://t.me/joinchat/AAAAAEWbURDTisztrTcwqA) | 科普视频系列：[无线技术](https://www.youtube.com/watch?v=JVh6sUHRxjg&list=PLqybz7NWybwULxQ2xMyUND_x2ziMLbn7R)、[显示技术](https://www.youtube.com/watch?v=MVVQl0gJH-U&list=PLqybz7NWybwWcl_s-VLB_tXqbbBww6nbK)、[翻墙技术](https://www.youtube.com/watch?v=XKZM_AjCUr0&list=PLqybz7NWybwUgR-S6m78tfd-lV4sBvGFG)……    |
| [P3TERX ZONE](https://t.me/P3TERX_ZONE)                   | `P3TERX` 读作 Peter X。                                                                                                                                                                                                                                                                                              |
| [值物志](https://t.me/zhiwuzhi)                           | 分享各种值得尝试的事物：值得读的书、值得用的软件、值得看的电视剧……                                                                                                                                                                                                                                                   |
| [小虎の自留地](https://t.me/xiaohudejia)                  | 讨论家装心得或者有趣实用的家具电器。                                                                                                                                                                                                                                                                                 |
| [Leonn 的博客](https://t.me/liyuans)                      | 低价主机（VPS）资源。                                                                                                                                                                                                                                                                                                |
| [Yachen's Channel](https://t.me/yachme)                   | 刘亚晨是 Surge 的开发者\| [Yachen's Blog](https://yach.me/)                                                                                                                                                                                                                                                          |
| [BennyThink's Blog](https://t.me/mikuri520)               | 随便分享点什么，可能是某部剧，可能是某首歌，可能是一点点感动的瞬间，也可能是我最爱的老婆。                                                                                                                                                                                                                           |
| [MolunSays](https://t.me/molun)                           | 希冀笔尖之下，世界兴旺繁华 \| [博客](https://molun.net/)                                                                                                                                                                                                                                                             |
| [日常人间观察](https://t.me/hayami_kiraa)                 | 关心科技 / 人文 / 艺术 / 城市公共空间 / 女性和性别议题 / 劳工权益 / 个体叙事 / 电影 / 音乐 / 书 / 星星……                                                                                                                                                                                                             |
| [In The Flux](https://t.me/intheflux)                     | 关于文化、艺术与技术的信息流。                                                                                                                                                                                                                                                                                       |
| [为也行](https://t.me/weiyexing)                          | 「书籍 \| 电影 \| 资源 \| 技巧 \| 摸鱼图」大多原创，少部分转发。                                                                                                                                                                                                                                                     |
| [Jerry Zhang 的频道](https://t.me/JerryZhang)             | 在渥太华的长春人。 博客：[Overflow](https://jerryzhang.blog/)，向信息过载的世界大喊。 播客：[《科技聚变》](https://techfusionfm.com/)（TechFusion），我们谈论有关互联网的一切。                                                                                                                                      |
| [老人和糟](https://t.me/dizzyninja)                       | 没有频道简介，科技相关。                                                                                                                                                                                                                                                                                             |
| [Karen 医生の日常](https://t.me/KarenMoe)                 | 一个小医生的通讯站。不想出名，只传播一些信息和科普。谨慎关注，会发一些血淋淋的图片。                                                                                                                                                                                                                                 |
| [人海拾贝FlipRadio](https://t.me/flipradio)               | 翻转电台的 Channel，一些零零散散的要分享的东西。                                                                                                                                                                                                                                                                     |
| [Find Blog](https://t.me/FindBlog)                        | 发现优秀的博客与创作者。                                                                                                                                                                                                                                                                                             |
| [TomBen’s Web Excursions](https://t.me/tombenor)          | PhD Student、Productivity Enhancer、Writing Enthusiast [博客](https://blog.retompi.com/) \| [少数派](https://sspai.com/u/tomben/updates)                                                                                                                                                                             |
| [熊言熊语](https://t.me/kaopubear)                        | 「熊言熊语」是一档关注学习分享和知识科普的 [播客](https://podcast.kaopubear.top/) 栏目，我们希望用声音记录改变与成长。思考问题的熊和他的朋友们一起聊学习工作、聊科研科普。 [博客](https://kaopubear.top/blog/) \| [Newsletter](https://top.us10.list-manage.com/subscribe?u=82fe193c38ed8c2752100685b&id=9dc03f5017) |
| [Hell Cell 功能教学](https://t.me/HellCellZC123)          | 通过 [YouTube](https://www.youtube.com/channel/UCgjdfJbXYaFfC3Wxump37Mg) 视频讲解一些实用软件那些有用有趣的功能。                                                                                                                                                                                                    |
| [The Sociologist](https://t.me/thesoc)                    | 我们只谈论记忆，因为不再有记忆。                                                                                                                                                                                                                                                                                     |
| [每日摄影观察](https://t.me/cnphotog_collect)             | 一个不严肃的摄影频道。                                                                                                                                                                                                                                                                                               |
| [中國家地理雜誌中文版](https://t.me/natgeomedia)          | Hi 探險家，和國家地理一起探索世界吧！                                                                                                                                                                                                                                                                                |

- [EdNovas 的小站](https://t.me/ednovas2) `@ednovas2`
  网站：[ednovas.xyz](https://www.ednovas.xyz/)
  导航：[navigate.ednovas.xyz](https://navigate.ednovas.xyz/)
- [gledos 的微型博客](https://t.me/gledos_microblogging) `gledos_microblogging`
  请记住我们，因我们也在这世上爱过和笑过。
- [Route 66 Blog](https://t.me/landofmaplex) `landofmaplex`
  [网站](https://route66x.com/)：留学、移民、程序员、死磕北美、加拿大、美国、跑路、移民生活。

[中文独立博客列表](https://github.com/timqian/chinese-independent-blogs) by timqian

#### 🔔 RSS

| 频道                                        | 详情                                                            |
| ------------------------------------------- | --------------------------------------------------------------- |
| [RSSHub 布告栏](https://t.me/awesomeRSSHub) | 万物皆可 RSS。                                                  |
| [All About RSS](https://t.me/aboutrss)      | 关于 RSS 技术的应用、周边、介绍、方法、教程、指南、讨论、观点。 |
| [RSS 频道收集](https://t.me/rss_channels)   | 收集推送 RSS 的频道，把 TG 变成 RSS 阅读器。                    |

#### 🎙 播客

采用 RSS 订阅的播客，永远都不会过时。

| 频道                                                          | 详情                                                                                                                                                                                                                             |
| ------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [「利器x播客」计划](https://t.me/liqixpodcast)                | [官网](https://liqixpodcast.typlog.io/)                                                                                                                                                                                          |
| [播客先声](https://t.me/voicefirst)                           | 分享关于播客的一切。by Zac                                                                                                                                                                                                       |
| [中文播客精选](https://t.me/greatpodcasts)                    | 分享精选优质中文播客，目前推荐单期节目为主。by [白昼电台](https://day.pcast.me/) 的主播 Stella                                                                                                                                   |
| [Your Daily Dose of Podcast](https://t.me/daily_dose_podcast) | 每天推荐一集让人心潮澎湃、若有所思、打开新世界大门的播客节目。by 穿堂风 推荐的播客会同步更新在 [Medium](https://medium.com/@chuantangfeng) [我在豆瓣上分享了 400 集播客节目，有什么用？](https://www.douban.com/note/776029208/) |
| [交差点](https://t.me/jiaochadian)                            | Technology alone is not enough.                                                                                                                                                                                                  |
| [不客观 Not Objective](https://t.me/notobjective)             | 一档搭建在 Telegram 的简易播客，纯主观感受。by [郝海龙](https://haohailong.net/)                                                                                                                                                 |
| [白昼电台 The Day](https://t.me/baizhoutheday)                | 黑夜已深，白昼将近，我们就当脱去暗昧的行为，带上光明的兵器。                                                                                                                                                                     |
| [维生素 E](https://t.me/vitamineEpodcast)                     | 经济学与哲学知识分享。                                                                                                                                                                                                           |
| [Go 夜聊](https://t.me/talkgofm_channel)                      | 一档由杨文和欧长坤主持的针对 Go 语言的播客节目                                                                                                                                                                                   |
| [阿乐杂货铺](https://t.me/hoiale)                             | 这里每日推送小人物播客及播客周边；职业发展、自我成长、读书电影、海外工作与生活碎片。                                                                                                                                             |

#### 🎵 音乐

| 频道                                             | 详情                                                         |
| ------------------------------------------------ | ------------------------------------------------------------ |
| [知音](https://t.me/Musiccnchannel)              | 👍 发一些关于音乐的东西。                                     |
| [Imusic](https://t.me/Imusic_zz)                 | 音乐，就是理想的挽歌，年代久远，依然飘扬。                   |
| [杂鱼Music Channel](https://t.me/ZAYU_music)     | 我相信，爱音乐的人都有着一颗柔软的心。                       |
| [音乐世界](https://t.me/lumingguandj)            | 温柔被我唱成了歌，伴你人山人海不停留。                       |
| [心声：音乐分享频道](https://t.me/yyfx_p)        | 分享一些能引起共鸣的音乐。                                   |
| [每日一歌](https://t.me/dailymusich)             | 愿你也能在这里找到属于你自己的共鸣。                         |
| [Classical Music](https://t.me/exploreclassical) | 一起来听古典音乐吧。                                         |
| [蛙音通讯](https://t.me/wahyin)                  | Feels wonderful again man.                                   |
| [无损音乐频道](https://t.me/undamaged_music)     | 分享无损音乐、高品质音乐、原碟整轨分轨音频。                 |
| [浦镇青年](https://t.me/Asdzxcyuihjkqwe)         | 李志。                                                       |
| [崔健无损](https://t.me/CuiJianDiscography)      | 中国大陆摇滚先驱者。                                         |
| [音乐分享频道](https://t.me/yyfx_pd)             | 一般为无损音乐。                                             |
| [音乐屋](https://t.me/Music_home0)               | 发现音乐新世界：live、黑胶、磁带                             |
| [CXPLAY MUSIC](https://t.me/cxplaymusic)         | 单曲音乐试听，专辑存放在 [这](https://t.me/musicsharetime)。 |

下载音乐，还可以查阅下文中提到的音乐机器人。

#### 🏫 读书

人类的悲喜并不互通，但读书是走向共同理解的捷径。

| 频道                                                       | 详情                                                                                                                   |
| ---------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------- |
| [Word Power Made Easy](https://t.me/pieroots)              | 利用词根（原始印欧语、拉丁语、古希腊语）学习英语单词。                                                                 |
| [英语精读学习](https://t.me/englishlearn2020)              | 夜空中最亮的星，就是你自己！我们一起精读英语，一起进步，遇见更好的自己吧！资料不定时更新哟！                           |
| [ENGLISH PODCASTS](https://t.me/Podcast_English_listening) | INFINITY PODCASTS CHANNEL WITHOUT ANY LIMITS.                                                                          |
| [中文社科讲座资讯](https://t.me/chwebinars)                | 一个讲座信息聚合和 PPT 共享平台。                                                                                      |
| [ReadFine 电子书屋](https://t.me/Readfine)                 | 致力于电子书分享的读书频道。EPUB 电子书一站式阅读体验（包括豆瓣评分、书籍简介、封面截图），一键下载，享受读趣。        |
| [The Economist Sharing Channel](https://t.me/sharingte)    | Sharing the Economist and E-books every week.                                                                          |
| [什么书值得读](https://t.me/zreadpush)                     | 仅推送某亚原版资源，可同时下载 `.azw3` `.epub` `.mobi` 的电子书。                                                      |
| [好书分享频道](https://t.me/haoshufenxiang)                | 学习，是一辈子的大事。                                                                                                 |
| [小声读书](https://t.me/weekly_books)                      | 一个探索数字阅读可能性和未来的开放项目，致力于打破信息茧房，挖掘价值信息。                                             |
| [值得一看的文章](https://t.me/readgoods)                   | 阅读更少，收获更多。                                                                                                   |
| [云上报刊亭](https://t.me/magazinesclub)                   | 英文报刊杂志、电子书、报纸和外文杂志精选。                                                                             |
| [Λ-Reading](https://t.me/GoReading)                        | 分享书和阅读、认知科学、科技哲学、新科技以及其它给生活带来一丝美好的事物 \| [Newsletter](https://rizime.substack.com/) |
| [臭（xiù）文字](https://t.me/SniffAtWord)                  | 诗歌频道；我是一个嗅觉特别发达的人，你说，然而，没有一种艺术可供我的鼻子用武，只有生命可以。                           |
| [已有丹青約［書畫］](https://t.me/CultureTG)               | 高清油画档案（超过两万张）。                                                                                           |
| [阿银书屋](https://t.me/maofanjd)                          | 偶尔更新，没事来看看。                                                                                                 |
| [红楼梦](https://t.me/DreamOfRedMansions)                  | 每日一章 💞DreamOfRedMansions。                                                                                         |

插播一个免费的广告：学英语，推荐购买郝海龙老师的[《英语自学手册》](https://sspai.com/series/77)（￥119）。

#### 🚀 翻墙

**软件**

| 频道                                                                | 详情                                                                                                                                                               |
| ------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [Clash .NET 公告](https://t.me/ClashDotNetFrameworkAnncmnt)         | 👍/ A Clash GUI Proxy For Windows Based On .NET 5                                                                                                                   |
| [Fndroid 的日常](https://t.me/fndroid_news)                         | 👍 Clash for Windows                                                                                                                                                |
| [Clash for Android Channel](https://t.me/joinchat/uCUxZwHNjZxlYThl) | A Graphical user interface of Clash for Android                                                                                                                    |
| [Surfboard News](https://t.me/surfboardnews)                        | 安卓专享的翻墙客户端，但不支持 SSR。 [用户手册](https://manual.getsurfboard.com/)                                                                                  |
| [SagerNet Apks](https://t.me/SagerNetApks)                          | 支持 SOCKS、HTTP(S)、Shadowsocks、ShadowsocksR、VMess、VLESS、Trojan……等协议 [SagerNet 官网](https://sagernet.org/)                                                |
| [AnXray](https://t.me/AnXray)                                       | Another Xray for Android [GitHub](https://github.com/XTLS/AnXray)                                                                                                  |
| [Shadowrocket News](https://t.me/ShadowrocketNews)                  | iOS 上小火箭                                                                                                                                                       |
| [Quantumult News](https://t.me/quantumultappnews)                   | Quantumult 的非官方频道。                                                                                                                                          |
| [Quantumult X News](https://t.me/QuanXNews)                         | 此频道用于发布 Quantumult 与 Quantumult X 的相关资讯。                                                                                                             |
| [迷雾通（Geph）](https://t.me/gephannounce)                         | 与众不同的开源翻墙软件，提供完全免费的中速浏览，够浏览新闻、查邮件、看标清视频等。超快速度的付费 Plus 账号仅需 €5/月。截至 2021 年 5 月 29 日，暂不支持 iOS 设备。 |

**协议 & 脚本 & 规则**

| 频道                                                  | 详情                                                                                                                                                                   |
| ----------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [V2Fly](https://t.me/V2Fly)                           | Shadowsocks 是一个纯粹的代理工具，而 V2Ray 定位为一个平台，任何开发者都可以利用 V2Ray 提供的模块开发出新的代理软件。by [新 V2Ray 白话文指南](https://guide.v2fly.org/) |
| [ACL4SSR](https://t.me/ACL4SSR)                       | https://github.com/ACL4SSR/ACL4SSR 官方频道。                                                                                                                          |
| [QuanX & Surge & Loon 脚本收集](https://t.me/NobyDa)  | 各种脚本。                                                                                                                                                             |
| [QuantumultX 教程&API&解析器](https://t.me/QuanX_API) | 如题。                                                                                                                                                                 |
| [Cool Scripts](https://t.me/cool_scripts)             | QuanX, Loon, Surge, JsBox, Pythonista, Scriptable, Shortcuts 等脚本分享。                                                                                              |
| [DivineEngine](https://t.me/DivineEngine)             | 神机规则                                                                                                                                                               |

**评测**

| 频道                                         | 详情                                                                                                                             |
| -------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| [毒药机场评测](https://t.me/DuyaoSS)         | 由于大陆地区网络环境十分复杂，测速不代表推荐。另外，有些机场会泄露个人信息，选购时多加搜索或者进入机场用户群打探打探。           |
| [品云☁️测速](https://t.me/PinYunPs)           | 细品各种云☁️。[PinYun](https://52.mk/) is a non-profit organization dedicated to making the internet a better place for everyone. |
| [科学上网与机场观察](https://t.me/jichangtj) | 科学上网与机场相关观察、点评、随想和新闻资讯。                                                                                   |

**关联阅读**

- [番茄食用指南（科学上网教程） | 庭说](https://tingtalk.me/fq)
- [番茄种植指南（梯子搭建教程） | 庭说](https://tingtalk.me/fq-diy/)

#### 🗄️ 搬运

| 频道                                          | 详情                                                                                                                                 |
| --------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| [煎蛋无聊图](https://t.me/jandan_pic)         | 自动抓取煎蛋首页推荐无聊图及其评论。                                                                                                 |
| [内涵段子：皮一下](https://t.me/duanzitg)     | 如题。                                                                                                                               |
| [美图与沙雕](https://t.me/shadiaotu)          | 如题。                                                                                                                               |
| [糗事百科](https://t.me/qiushibaike)          | 如题。                                                                                                                               |
| [心惊报](https://t.me/xinjingdaily)           | 又一个沙雕图频道，每日随缘更新。                                                                                                     |
| [你知道的太多了](https://t.me/uknow2much)     | 不定期发布和转载各类不一定靠谱的内幕、流言蜚语、小知识等。                                                                           |
| [蛋挞报](https://t.me/pincongessence)         | 分享阅读体验。                                                                                                                       |
| [微信搬运工](https://t.me/WeChatEssence)      | 有些微信的内容分享了之后就和谐了，本频道可以做个备份，以及丰富电报上的中文内容（不可否认还是有很多非政治的优质内容在微信公众号里）。 |
| [微博精选](https://t.me/weibo_read)           | 来自微博的文章、资源和观点。                                                                                                         |
| [豆瓣精选](https://t.me/douban_read)          | 豆瓣书影音，以及相关讨论。                                                                                                           |
| [鹅组精选](https://t.me/douban_goose)         | [豆瓣鹅组](https://www.douban.com/group/blabla) 非官方搬运。                                                                         |
| [即刻精选](https://t.me/jike_collection)      | 精选即刻 app 热门话题更新。我的即刻 ID 是 [Dr_Ting](https://m.okjike.com/users/Zhong_Waiting)。                                      |
| [你不知道的内幕消息](https://t.me/inside1024) | 同时抓取来自即刻 app 的 #大公司的负面新闻。                                                                                          |
| [Matters 閲讀精選](https://t.me/MattersHub)   | matters.news 一個自主、永續、有價的創作與公共討論空間。                                                                              |

#### 🆓 资源

| 频道                                                | 详情                                                                                                                                                      |
| --------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Google Play 限免信息](https://t.me/playsales)      | 不定时推介 Play Store 上的限免游戏和 App。                                                                                                                |
| [Price Tag](https://t.me/appfans)                   | 推荐 App 限免降价，推送好物好券。                                                                                                                         |
| [纯粹的 App Store 应用推荐](https://t.me/app_store) | iOS 实用免费、精选限免、优质冰点应用推荐。                                                                                                                |
| [反斗限免](https://t.me/apprcn)                     | 这里有反斗软件和反斗限免的文章更新。更新频繁高。                                                                                                          |
| [如有乐享](https://t.me/ruyoblog)                   | 更新 [如有乐享博客](https://51.ruyo.net/) 的内容：云服务器、优惠活动、羊毛信息以及各种 Bug。                                                              |
| [iShare News](https://t.me/iShareNews)              | 一个没有简介的资源分享频道。                                                                                                                              |
| [Zapro Notice](https://t.me/zaproshare)             | 软件分享。                                                                                                                                                |
| [App 喵](https://t.me/appmew)                       | 破解软件资源共享。                                                                                                                                        |
| [Google Drive 资源](https://t.me/gdurl)             | 各种 Google Drive 资源，包括电影、电子书、无损音乐等，10 万+ 关注。                                                                                       |
| [Google Voice 靓号](https://t.me/voice_google)      | 一个 GV 卖家。                                                                                                                                            |
| [Windows 10 激活码分享](https://t.me/win10keymaxs)  | 🤫                                                                                                                                                         |
| [Office Tool Plus](https://t.me/otp_channel)        | [Office Tool Plus](https://otp.landian.vip/zh-cn/) 是一个用于部署、激活Office、Visio、Project 的小工具。借助本工具，你可以快速地完成各项Office 部署工作。 |
| [你有一个打折需要了解](https://t.me/SteamNy)        | 分享 Steam 的周榜、折扣、资讯、喜加一等。                                                                                                                 |
| [52 破解信息](https://t.me/wuaipojie)               | 吾爱破解。                                                                                                                                                |

- [擅长搜索的高木同学](https://t.me/gaomutongxue) `gaomutongxue`

- [黑科技软件资源分享](https://t.me/kkaifenxiang) `kkaifenxiang`
  分享免费实用高效率网络资源、黑科技软件、实用黑技巧。

- [Discover good software](https://t.me/ksc666) `ksc666`
  分享 Magisk、Riru、LSPosed、虚拟框架、Xposed 模块、Magisk 模块、Android、Windows……等软件。

- [破解安卓 VPN 软件](https://t.me/vpn_cracked) `vpn_cracked`

  发布原创破解的 VPN 和各种软件，以及分享各类资源，多位安卓逆向大佬坐镇。

- [万能福利吧](https://t.me/wnflb) `@wnflb`
  分享有趣的信息，包含网站、活动、网购、下载综合症、好孩子看不见等福利。

#### 🎞️ 视频

**电影 / 剧集**

- [四库全书](https://t.me/video4lib) `video4lib` 👍
  一个不断收集互联网有价值内容的企划。
- [电影频道](https://t.me/TGDY188) `TGDY188`
  精选国内外高分电影。
- [华联社电影频道](https://t.me/Cctv365) `Cctv365`
- [霸王龙发布频道](https://t.me/T_rex2333) `T_rex2333`
  专注于韩美剧，选取优质影片源。
- [苍炎影院](https://t.me/cangyanmovie) `cangyanmovie`
  分享最新最热门的优质电影。
- [双语短视频合集](https://t.me/english_bilingual) `english_bilingual`
  学习英语，了解世界。

**动漫**

- [海贼王更新提醒](https://t.me/tingtalk_op) `tingtalk_op`
  [@TingTalk](https://t.me/tingtalk) 子频道，试运营。由初中开始追 One Piece 的 Dr_Ting 创建，
- [Rick and Morty](https://t.me/tingtalk_rm)
  [@TingTalk](https://t.me/tingtalk) 子频道，试运营。曾经把《瑞克和莫蒂》作为练口语的 [素材](https://tingtalk.me/rick-and-morty-subtitles/)，听了上百遍，但效果甚微，Wubba Lubba Dub-Dub。

**下载站**

- [Odyssey+](https://t.me/odysseyplus) `odysseyplus`
  [公益服食用指南](https://odysseyplus.notion.site/odysseyplus/f54b8a881f7044619151c3e55bdfaeb8)。

- [PT 资讯频道](https://t.me/PrivateTrackerNews) `@PrivateTrackerNews`
  Private Tracker 资讯以及开放注册信息推送；PT 可以简单理解为私有化的 BT。

- [Sync 资源更新](https://t.me/shenkey) `@shenkey`
  只发 key。

- [电视机顶盒 & 手机影视 App](https://t.me/tvbox001) `@tvbox001`

  可看港台电视直播、美剧等。

#### 😺 其它

**软件**

- [Aria2 Channel](https://t.me/Aria2_Channel) `@Aria2_Channel`
  Aria2 完美配置、Pro Docker、Pro Core、一键安装管理脚本增强版 (GNU/Linux)。

**未分类**

- [NBA](https://t.me/tingtalk_nba) `tingtalk_nba`
  [@TingTalk](https://t.me/tingtalk) 子频道，试运营。从高中开始只练跳投，因此严重偏科，不会突破，不会抢篮板，不会防守，但崴脚少了，命中率高了。

| 频道                                                    | 详情                                                                                                                                                                                   |
| ------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [iYouPort](https://t.me/iyouport)                       | IYP 不是过眼云烟的新闻网站，我们提供实战能力，这里是值得您反复回看的档案室。                                                                                                           |
| [安全上网注意事项](https://t.me/anquanshangwang)        | 转载一些关于安全上网的文章，这些文章都比较浅显。                                                                                                                                       |
| [博海拾贝](https://t.me/bohaishibei)                    | [博海拾贝](https://bh.sb/) 的网站：[bh.sb](http://bh.sb/)                                                                                                                              |
| [回形针PaperClip & 灵光灯泡](https://t.me/papercliphub) | 回形针内容推送。                                                                                                                                                                       |
| [合租](https://t.me/hezu2)                              | Netflix、YouTube、Spotify、Office 365、HBO、Apple、Surge……                                                                                                                             |
| [History](https://t.me/History)                         | Digging Past. Photos from Past who shaped today.                                                                                                                                       |
| [每日无数猫](https://t.me/miaowu)                       | 让我们打造一个全是猫的世界！ฅ•ﻌ•ฅ                                                                                                                                                      |
| [NS 新闻转报](https://t.me/SwitchNewCN)                 | 任天堂（Nintendo）相关的新闻。                                                                                                                                                         |
| [基督讲道](https://t.me/TelBaptist)                     | 基督讲道资源频道。                                                                                                                                                                     |
| [就要造反](https://t.me/ZaoFaner)                       | 此频道立足生活，以非常古怪的文字风格进行生存经验书写，绘制景观与消费社会中极具现实性的个案，以此为个体提供可操的、创造性的抵制策略与造反计谋。为一切造反者辩护，为所有无用与丰饶辩护。 |
| [残障之声](https://t.me/life_with_disabilities)         | 在态度和环境障碍相互作用存在的情况下，提供合理便利是全社会需要一同去解决的问题，残障人士应当理直气壮地要求这种权利和便利，去定义一个无障碍的社会。                                     |

- [每日一句](https://t.me/meiriyiju) `meiriyiju`
  每天一句心灵鸡汤，配上必应每日壁纸。
- [Leanote](https://t.me/leanote) `leanote`
  今天的定位（今天的是什么日子）：单向历、mono日签等

## 附录

### Reference

### 版权声明

本文原载于https://all2doc.com
